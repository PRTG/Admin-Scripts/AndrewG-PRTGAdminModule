<# 
       Klas.Pihl@atea.se 2018
       Fetch files from PRTG core server system information database and append data with devicename and when data was written. 
       Gives the operator option to do select "class".
       The export is crude to a big json that is possible to import in Excel, powerBI...
#>
param (
       $PRTGCoreServer = 'prtg01',
       $ExportPath = 'c:\storage\'
)
 try {
	# Powershellmodule for PRTG https://github.com/AndrewG-1234/PRTG . uses API
	Import-Module d:\powershell\PRTGAdminModule.psm1   
} catch {
	write-error $error[0]
	write-output 'You can find the PRTGAdminModule.psm1 at https://github.com/AndrewG-1234/PRTG'
	exit
} 

$path = ('\\{0}\c$\ProgramData\Paessler\PRTG Network Monitor\System Information Database' -f $PRTGCoreServer)
$outFile = join-path $ExportPath -childpath ('cmdb-{0}.json' -f (get-date -Format yyMMdd-hhmm))
 
#possible selection of class
$path = (Get-ChildItem $path -Directory | select Name,fullname | Out-GridView -PassThru -Title 'Select class(es)').fullname
if(!($path)) {Write-Error "No class selected";exit}
 
$dBfiles = $path | %{Get-ChildItem  -Path $_ -Recurse | where {$_.PSIsContainer -ne $true}}
$data = $null
 
#Get devicename and deviceID
Write-Host -ForegroundColor Cyan "Get Device Name from PRTG API"
$PRTGDevices = Get-prtgDevicesInGroup | select id,device,host
 
#From filename of each deviceID adds attributes. 
Write-Host -ForegroundColor Cyan "Adds attributes from imported db, can take some time."
foreach ($file in $dBfiles)
    {
    $sysinfo = (Get-Content $file.FullName | ConvertFrom-Json).data|select deviceID,deviceName,LastData,class,*
    $rows = $sysinfo.displayname.count
    if($rows -gt 0) {
                $i = 0
                do                {
                $sysinfo[$i].deviceID =  $file.Name.Split(' ')[1].split('.')[0]
                $sysinfo[$i].class =  $file.Name.Split(' ')[1].split('.')[1]
                $sysinfo[$i].LastData = $file.LastWriteTime.ToString()
                $sysinfo[$i].devicename = ($PRTGDevices | where {$_.id -eq $sysinfo[$i].deviceID}).device
                if(!($sysinfo[$i].devicename)) {$sysinfo[$i].devicename = 'Removed device'}
                $i++
                }
                until ($i -eq $rows)
    }
    $data+=$sysinfo
    }
    Write-Host -ForegroundColor Cyan "Exporting file to $outFile"
$data | ConvertTo-Json | Out-File $outFile  
 
